-- bit library extracted from Thomas Farr's Lua String Utils library
-- License is stored at /LICENSE
local bit = {}
bit["bnot"] = function(n)
    local tbl = bit.tobits(n)
    local size = math.max(table.getn(tbl), 32)
    for i = 1, size do
        if(tbl[i] == 1) then
            tbl[i] = 0
        else
            tbl[i] = 1
        end
    end
    return bit.tonumb(tbl)
end
bit["band"] = function(m, n)
    local tbl_m = bit.tobits(m)
    local tbl_n = bit.tobits(n)
    bit.expand(tbl_m, tbl_n)
    local tbl = {}
    local rslt = math.max(table.getn(tbl_m), table.getn(tbl_n))
    for i = 1, rslt do
        if(tbl_m[i]== 0 or tbl_n[i] == 0) then
            tbl[i] = 0
        else
            tbl[i] = 1
        end
    end
    return bit.tonumb(tbl)
end
bit["bor"] = function(m, n)
    local tbl_m = bit.tobits(m)
    local tbl_n = bit.tobits(n)
    bit.expand(tbl_m, tbl_n)
    local tbl = {}
    local rslt = math.max(table.getn(tbl_m), table.getn(tbl_n))
    for i = 1, rslt do
        if(tbl_m[i]== 0 and tbl_n[i] == 0) then
            tbl[i] = 0
        else
            tbl[i] = 1
        end
    end
    return bit.tonumb(tbl)
end
bit["bxor"] = function(m, n)
    local tbl_m = bit.tobits(m)
    local tbl_n = bit.tobits(n)
    bit.expand(tbl_m, tbl_n)
    local tbl = {}
    local rslt = math.max(table.getn(tbl_m), table.getn(tbl_n))
    for i = 1, rslt do
        if(tbl_m[i] ~= tbl_n[i]) then
            tbl[i] = 1
        else
            tbl[i] = 0
        end
    end
    return bit.tonumb(tbl)
end
bit["brshift"] = function(n, bits)
    bit.checkint(n)
    local high_bit = 0
    if(n < 0) then
        n = bit.bnot(math.abs(n)) + 1
        high_bit = 2147483648
    end
    for i=1, bits do
        n = n/2
        n = bit.bor(math.floor(n), high_bit)
    end
    return math.floor(n)
end
bit["blshift"] = function(n, bits)
    bit.checkint(n)
    if(n < 0) then
        n = bit.bnot(math.abs(n)) + 1
    end
    for i=1, bits do
        n = n*2
    end
    return bit.band(n, 4294967295)
end
bit["bxor2"] = function(m, n)
    local rhs = bit.bor(bit.bnot(m), bit.bnot(n))
    local lhs = bit.bor(m, n)
    local rslt = bit.band(lhs, rhs)
    return rslt
end
bit["blogic_rshift"] = function(n, bits)
    bit.checkint(n)
    if(n < 0) then
        n = bit.bnot(math.abs(n)) + 1
    end
    for i=1, bits do
        n = n/2
    end
    return math.floor(n)
end
bit["tobits"] = function(n)
    bit.checkint(n)
    if(n < 0) then
        return bit.tobits(bit.bnot(math.abs(n)) + 1)
    end
    local tbl = {}
    local cnt = 1
    while (n > 0) do
        local last = math.fmod(n,2)
        if(last == 1) then
            tbl[cnt] = 1
        else
            tbl[cnt] = 0
        end
        n = (n-last)/2
        cnt = cnt + 1
    end
    return tbl
end
bit["tonumb"] = function(tbl)
    local n = table.getn(tbl)
    local rslt = 0
    local power = 1
    for i = 1, n do
        rslt = rslt + tbl[i]*power
        power = power*2
    end
    return rslt
end
bit["checkint"] = function(n)
    if(n - math.floor(n) > 0) then
        error("trying to use bitwise operation on non-integer!")
    end
end
bit["expand"] = function(tbl_m, tbl_n)
    local big = {}
    local small = {}
    if(table.getn(tbl_m) > table.getn(tbl_n)) then
        big = tbl_m
        small = tbl_n
    else
        big = tbl_n
        small = tbl_m
    end
    for i = table.getn(small) + 1, table.getn(big) do
        small[i] = 0
    end
end
return bit
